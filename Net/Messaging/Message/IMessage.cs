﻿/*
 *  Author  : Nahuel Taibo
 *  email   : nahueltaibo@gmail.com
	The idea of this library is to provide a lightweight messaging protocol to communicate through whatever two elements you want
	Serial connection
	ArduinoAccessory connection
	etc
	The messages has this structure: |type|payloadLength|payload|
	+ Type: A unsigned byte used by the protocol
	+ PayloadLength: A byte used by the protocol to keep track of the size of the payload (NOT the size of the full message)
	+ Payload: It is a byte[] and the actual payload of the message.
	
	The library provides methods to access different types on the payload, so you should respect the order of the parameters and
	from each end you can pop each parameter, following the same order you used to push them to the payload.
	Example:
	//This code shows how to create a message
	int intVar = -2000;
	byte byteVar = 10;
	char charVar = 'A';
	Message message;

	message.push(intVar);
	message.push(byteVar);
	message.push(charVar);
	message.push(stringVar);

	//At this poing, the message bytes are:
	----------
	01	|0x03|	Message type byte
	----------
	02	|0x03|	Payload size
	----------
	03	|    |	First byte of the int param
	04	|    |	Second byte of the int param	
	----------
	05	|    |	Byte param
	----------
	06	|    |	Char param
	----------


	//This code shows how to handle a received message
	Message message = |receivedMessage|;

	int intVar = message.popInt();
	byte byteVar = message.popByte();
	char charVar = message.popChar();

	//And thats it, now you can do what ever you whant with those parameters

	//I recomend inheriting Message class, and providing a simpler way of accessing the parameters, so you can just call myMessage.intParam
	//or something like that. I'll create an example of that too.
*/

using System.IO;

namespace Message
{
    internal interface IMessage
    {
        MessageType Type { get; }
        short PayloadLength { get; }
        short TotalLength { get; }

        /// <summary>
        ///     Returns the byte[] array representing this message
        /// </summary>
        byte[] Bytes { get; }

        bool PushByte(int arduinoByte);
        bool PushInt(short arduinoInt);
        bool PushChar(char arduinoChar);
        short PopByte();
        short PopInt();
        char PopChar();

        /// <summary>
        ///     Resets the message Payload , so Payload is 0 and Pos is 0
        /// </summary>
        void ResetPayload();

        void Send(Stream outputStream);
    }
}