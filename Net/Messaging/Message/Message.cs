﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Infrastructure;

namespace Message
{
    public class Message : IMessage
    {
        //For some reason, sending the message fails if it is more than 64 bytes in total (so 62 of payload)
        public const short MaxMessageLength = 64;
        public const short HeaderLength = 2;
        public const short MaxPayloadLength = MaxMessageLength - HeaderLength;
        //Arduino Data type sizes
        public const short ArduinoByteSize = 1;
        public const short ArduinoIntSize = 2;
        public const short ArduinoCharSize = 1;
        public const short ArduinoIntMin = -32768;
        public const short ArduinoIntMax = 32767;
        private short pos;
        private readonly MessageType messageType;
        private readonly List<byte> payload;

        public Message(MessageType messageType)
        {
            this.messageType = messageType;
        }

        /// Creates a message from a raw buffer
        public Message(byte[] buffer)
        {
            //I don't do sanity check here, since Arduino is using unsigned bytes for the messages, so
            // there should be no way it sends bigger values.
            messageType = (MessageType) buffer[0];
            var payloadLength = buffer[1];
            payload = new List<byte>(payloadLength);

            for (var i = 0; i < payloadLength; i++)
            {
                payload.Add(buffer[i + 2]);
            }
        }

        /// <summary>
        ///     Reads from the DataInputStream the contents of the Message
        /// </summary>
        /// <param name="inputStream"></param>
        public Message(Stream inputStream)
        {
            //Wait and read the "type" byte
            Log.Debug("Waiting for type byte...");
            short rcv = (byte) inputStream.ReadByte();

            //Check that the InputStream is still available
            if (rcv < 0)
            {
                throw new IOException("InputStream closed.");
            }

            messageType = (MessageType) rcv;

            //Wait and read the "payloadLength" byte
            Log.Debug("Waiting for payloadLength byte...");

            rcv = (short) inputStream.ReadByte();

            //Check that the InputStream is still available
            if (rcv < 0)
            {
                throw new IOException("InputStream closed.");
            }
            var payloadLength = rcv;

            //Create the payload container
            payload = new List<byte>(payloadLength);

            //Wait and read the "payload" bytes
            Log.Debug(string.Format("Waiting for %d payload bytes...", payloadLength));
            for (var i = 0; i < payloadLength; i++)
            {
                rcv = (short) inputStream.ReadByte();

                //Check that the InputStream is still available
                if (rcv < 0)
                {
                    throw new IOException("InputStream closed.");
                }
                payload[i] = (byte) rcv;
            }

            Log.Debug(string.Format("Message received: %s", this));
        }

        public MessageType Type
        {
            get { return messageType; }
        }

        public short PayloadLength
        {
            get { return (short) payload.Count; }
        }

        public short TotalLength
        {
            get { return (short) (payload.Count + HeaderLength); }
        }

        public byte[] Bytes
        {
            get
            {
                var buffer = new byte[payload.Count + HeaderLength];

                buffer[0] = (byte) messageType;
                buffer[1] = (byte) payload.Count;

                for (var i = 0; i < payload.Count; i++)
                {
                    buffer[i + 2] = payload[i];
                }

                return buffer;
            }
        }

        public bool PushByte(int arduinoByte)
        {
            //Log.d(TAG, String.format("pushByte: %d", arduinoByte));

            if (arduinoByte < 0 || arduinoByte > 255)
            {
                throw new ArgumentException("arduinoByte argument should be between 0 and 255");
            }

            if (EnoughSpace(ArduinoByteSize))
            {
                payload.Add((byte) arduinoByte);
                return true;
            }
            return false;
        }

        public bool PushInt(short arduinoInt)
        {
            //Log.d(TAG, String.format("pushInt: %d", arduinoInt));

            if (EnoughSpace(ArduinoIntSize))
            {
                payload.Add((byte) (arduinoInt & 0xff));
                payload.Add((byte) ((arduinoInt >> 8) & 0xff));

                return true;
            }
            return false;
        }

        public bool PushChar(char arduinoChar)
        {
            //Log.d(TAG, String.format("pushChar: %d", arduinoChar));

            if (arduinoChar < 0 || arduinoChar > 255)
            {
                throw new ArgumentException("arduinoChar argument should be between 0 and 255");
            }

            if (EnoughSpace(ArduinoCharSize))
            {
                payload.Add((byte) arduinoChar);
                return true;
            }
            return false;
        }

        public short PopByte()
        {
            var result = payload[pos++];

            //Log.d(TAG, String.format("popByte: %d", result));

            return result;
        }

        public short PopInt()
        {
            var high = payload[pos++];
            var low = payload[pos++];

            var result = (short) ((high << 8) | low);

            //Log.d(TAG, String.format("popInt: %d", result));

            return result;
        }

        public char PopChar()
        {
            var result = (char) payload[pos++];

            //Log.d(TAG, String.format("popChar: %c", result));

            return result;
        }

        public void ResetPayload()
        {
            pos = 0;
            for (var i = 0; i < payload.Count; i++)
            {
                payload[i] = 0;
            }
        }

        public void Send(Stream outputStream)
        {
            if (outputStream != null)
            {
                //Get the message bytes
                var buffer = Bytes;

                //Write it to the requested Outputstream
                outputStream.Write(buffer, 0, buffer.Length);
            }
        }

        public bool EnoughSpace(short requestedSize)
        {
            var remaining = MaxMessageLength - payload.Count;

            var result = remaining >= requestedSize;

            if (!result)
            {
                Log.Error(string.Format("Not enought space in message for param. Requested: {0}, Remaining: {1}",
                    requestedSize,
                    remaining));
            }

            return result;
        }

        public override String ToString()
        {
            var result = new StringBuilder("|");
            var msgBytes = Bytes;

            for (var i = 0; i < msgBytes.Length; i++)
            {
                result.Append(string.Format("{0}|", msgBytes[i]));
            }

            return result.ToString();
        }
    }
}