namespace Message
{
    public enum MessageType
    {
        // Message types
        NoMessage = 0, // Not a valid message (Used when there is nothing to read)	
        Ping = 0x01, // PING request
        Pong = 0x02, // PING response
        Publish = 0x03, // Message containing the usable payload
        Disconnect = 0x04 // client is disconnecting
    }
}