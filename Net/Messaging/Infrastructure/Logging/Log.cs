﻿using System;

namespace Infrastructure
{
    public static class Log
    {
        public static void Debug(string message)
        {
            Console.WriteLine(message);
        }

        public static void Error(string message)
        {
            Console.WriteLine(message);
        }
    }
}