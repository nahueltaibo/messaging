﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Message.Tests
{
    [TestClass]
    public class MessageTests
    {
        [TestMethod]
        public void Message_CreatedWithValidMessageType()
        {
            var message = new Message(MessageType.Publish);
            Assert.AreEqual(MessageType.Publish, message.Type);
        }

        [TestMethod]
        public void Message_CreatedFromByteArray()
        {
            var message = new Message(new byte[] {(byte) MessageType.Ping, 0});
            Assert.AreEqual(MessageType.Ping, message.Type);
        }

        [TestMethod]
        public void PayloadLength_ReturnsExpectedValues()
        {
            var message = new Message(new byte[] {(byte) MessageType.Publish, 3, 1, 2, 3});
            Assert.AreEqual(3, message.PayloadLength);

            message = new Message(new byte[] {(byte) MessageType.Publish, 0});
            Assert.AreEqual(0, message.PayloadLength);
        }

        [TestMethod]
        public void TotalLength_ReturnsExpectedValues()
        {
            var message = new Message(new byte[] {(byte) MessageType.Publish, 3, 1, 2, 3});
            Assert.AreEqual(5, message.TotalLength);

            message = new Message(new byte[] {(byte) MessageType.Publish, 0});
            Assert.AreEqual(2, message.TotalLength);
        }

        [TestMethod]
        public void Bytes_ReturnsTheExpectedBytes()
        {
            var message = new Message(new byte[] {(byte) MessageType.Pong, 5, 1, 2, 3, 4, 5});
            var bytes = message.Bytes;

            Assert.AreEqual(MessageType.Pong, (MessageType) bytes[0]);
            Assert.AreEqual(5, bytes[1]);
            Assert.AreEqual(1, bytes[2]);
            Assert.AreEqual(2, bytes[3]);
            Assert.AreEqual(3, bytes[4]);
            Assert.AreEqual(4, bytes[5]);
            Assert.AreEqual(5, bytes[6]);
        }

        [TestMethod]
        public void PushByte_AddsTheByteToThePayload()
        {
            var message = new Message(new byte[] {(byte) MessageType.Publish, 0});

            message.PushByte(1);
            Assert.AreEqual(1, message.PayloadLength);
            Assert.AreEqual(1, message. Bytes[2]);

            message.PushByte(2);
            Assert.AreEqual(2, message.PayloadLength);
            Assert.AreEqual(2, message.Bytes[3]);

            message.PushByte(3);
            Assert.AreEqual(3, message.PayloadLength);
            Assert.AreEqual(3, message.Bytes[4]);
        }
        
        [TestMethod]
        public void PushInt_AddsTheTwoBytesToThePayload()
        {
            throw new NotImplementedException();
            //bool PushInt(short arduinoInt);
            
        }
        
        [TestMethod]
        public void PushChar_AddsTheCharToThePayload()
        {
            //bool PushChar(char arduinoChar);

            var message = new Message(new byte[] {(byte) MessageType.Publish, 0});

            message.PushChar('A');
            Assert.AreEqual(1, message.PayloadLength);
            Assert.AreEqual('A', (char) message.Bytes[2]);

            message.PushChar('9');
            Assert.AreEqual(2, message.PayloadLength);
            Assert.AreEqual('9', (char)message.Bytes[3]);

            message.PushByte('x');
            Assert.AreEqual(3, message.PayloadLength);
            Assert.AreEqual('x', (char) message.Bytes[4]);
        }
        
        [TestMethod]
        public void PopByte_ReturnsTheExpectedByte()
        {
            //short PopByte();
            var message = new Message(new byte[] {(byte) MessageType.Publish, 3, 1, 2, 3});

            Assert.AreEqual(1, message.PopByte());
            Assert.AreEqual(2, message.PopByte());
            Assert.AreEqual(3, message.PopByte());
            
            //A we have 3 bytes, the next call should throw an Exception
            Assert.ThrowsException(message.PopByte());
        }
        
        [TestMethod]
        public void PopInt_ReturnsTheExpectedInt()
        {
            throw new NotImplementedException();
            //short PopInt();
        }
        
        [TestMethod]
        public void PopChar_ReturnsTheExpectedChar()
        {
            //char PopChar();
            var message = new Message(new byte[] {(byte) MessageType.Publish, 3, (byte)'A', (byte)'5', (byte)'r'});

            Assert.AreEqual('A', message.PopChar());
            Assert.AreEqual('5', message.PopChar());
            Assert.AreEqual('r', message.PopChar());
            
            //A we have 3 bytes, the next call should throw an Exception
            Assert.ThrowsException(message.PopChar());
        }
        
        [TestMethod]
        public void ResetPayload_RestartsThePayload()
        {
            //void ResetPayload();
            var message = new Message(new byte[] {(byte) MessageType.Publish, 4, 1, 0, 1, (byte)'g'});

            Assert.AreEqual(1, message.PopByte());
            Assert.AreEqual(1, message.PopInt());

            message.ResetPayload();
            //We should get the first parameter again
            Assert.AreEqual(1, message.PopByte());
            Assert.AreEqual(1, message.PopInt());
            
            Assert.AreEqual('g', message.PopChar());
            
            //A we have 3 parameters, the next call should throw an Exception
            Assert.ThrowsException(message.PopByte());
        }

        [TestMethod]
        public void Send_WritesTheCorrectBytesToTheOutPutStream()
        {
            //void Send(Stream outputStream);
            //Tested with a MemoryStram, but should work with Any stream
            using(MemoryStream stream = new MemoryStream())
            {
                var message = new Message(new byte[] {(byte) MessageType.Publish, 4, -255, 0, 255, (byte)'g'});
                message.Send(stream);
                
                //Check the message type
                Assert.AreEqual((byte) MessageType.Publish, message.Bytes[0]);
                
                //Check the payload size
                Assert.AreEqual(4, message.Bytes[1]);

                //Check the parameters
                Assert.AreEqual((byte) -255, message.Bytes[2]);
                Assert.AreEqual((byte) 0, message.Bytes[3]);
                Assert.AreEqual((byte) 255, message.Bytes[4]);
            }
        }
        
        [TestMethod]
        public void EnoughSpace_ReturnsTheCorrectResponses()
        {
            throw new NotImplementedException();
            //public bool EnoughSpace(short requestedSize)
        }
    }
}