#ifndef Message_h
#define Message_h
#define DEBUG

/*
The idea of this library is to provide a lightweight messaging protocol to communicate through whatever two elements you want
Serial connection
ArduinoAccessory connection
etc
The messages has this structure: |type|payloadLength|payload|
+ Type: A unsigned byte used by the protocol
+ PayloadLength: A byte used by the protocol to keep track of the size of the payload (NOT the size of the full message)
+ Payload: It is a byte[] and the actual payload of the message.

The library provides methods to access different types on the payload, so you should respect the order of the parameters and
from each end you can pop each parameter, following the same order you used to push them to the payload.
Example:
//This code shows how to create a message
int intVar = -2000;
byte byteVar = 10;
char charVar = 'A';
Message message;

message.push(intVar);
message.push(byteVar);
message.push(charVar);
message.push(stringVar);

//At this poing, the message bytes are:
----------
01	|0x03|	Message type byte
----------
02	|0x03|	Payload size
----------
03	|    |	First byte of the int param
04	|    |	Second byte of the int param
----------
05	|    |	Byte param
----------
06	|    |	Char param
----------


//This code shows how to handle a received message
Message message = |receivedMessage|;

int intVar = message.popInt();
byte byteVar = message.popByte();
char charVar = message.popChar();

//And thats it, now you can do what ever you whant with those parameters

//I recomend inheriting Message class, and providing a simpler way of accessing the parameters, so you can just call myMessage.intParam
//or something like that. I'll create an example of that too.
*/

#include "Arduino.h"

// Message types
// note that 0x00 and 0x0F are reserved
// the ones implemented are marked with *
#define NO_MESSAGE		0x00  // Not a valid message (Used when there is nothing to read)	
#define PING			0x01  // PING request *
#define PONG			0x02  // PING response *
#define PUBLISH			0x03  // Message containing the usable payload
#define DISCONNECT		0x04  // client is disconnecting *
#define HEALTH_SECUENCE	0x05  // Defines the start of a sequence of messages used to test the connection between peers


//I dont know why but Android Accessory library seems to fail when messages are bigger than 64 bytes
//If I find the solution, then is just a matter of changing MAX_MESSAGE_LENGTH to a bigger value
#define MAX_MESSAGE_LENGTH 64

#define HEADER_LENGTH 2 

class Message
{
public:
	Message(byte theType);
	Message(byte* buffer, byte bufferLen);

	~Message();
	void debug(bool incoming);
	Message processPing();

	//Process the sequence of messages created to test the connection and the code
	void processHealthSecuence();

	byte getType();
	byte getLength();

	bool push(byte byteVal);
	bool push(int intVal);
	bool push(char charVal);

	byte popByte();
	int popInt();
	char popChar();

	//Returns true if there are unreaded bytes in the payload. (you should know what data type are those bytes)
	bool payloadAvailable();

	byte getTotalLength();
	byte* getBytes();

private:
	bool  enoughRoom(int requestedSize);
	byte type = NO_MESSAGE;
	byte payloadLength = 0;
	byte pos = 0;
	byte payload[MAX_MESSAGE_LENGTH - HEADER_LENGTH];
};

#endif
