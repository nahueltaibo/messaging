#include "Message.h"


Message::Message(byte theType)
{
	type = theType;

	//Clean the payloads buffer;
	for (int i = 0; i < sizeof(payload); i++){
		payload[i] = 0;
	}
}

Message::Message(byte* buffer, byte bufferLen) : Message(buffer[0])
{
	payloadLength = buffer[1];

	int i;

	//Load the payloads buffer;
	for (i = 0; i < sizeof(payload) && i < bufferLen; i++){
		payload[i] = buffer[i + HEADER_LENGTH];
	}

	//Clean the rest of the payload
	for (; i < sizeof(payload); i++){
		payload[i] = 0;
	}
}

Message::~Message()
{
}

byte Message::getType()
{
	return type;
}

byte Message::getLength()
{
	return payloadLength;
}

void Message::debug(bool incoming){
#ifdef DEBUG
	switch (type)
	{
	case PING: Serial.print("PING"); break;
	case PONG: Serial.print("PONG"); break;
	case PUBLISH: Serial.print("PUBLISH"); break;
	case DISCONNECT: Serial.print("DISCONNECT"); break;
	case HEALTH_SECUENCE: Serial.print("HEALTH_SECUENCE"); break;
	default: Serial.print("UNKNOWN"); break;
	}

	if (incoming)
	{
		Serial.print(" <- ");
	}
	else
	{
		Serial.print(" -> ");
	}

	Serial.print("|");
	Serial.print(type);
	Serial.print("|");
	Serial.print(payloadLength);
	Serial.print("|");

	for (int i = 0; i < payloadLength; i++)
	{
		Serial.print(payload[i]);
		Serial.print("|");
	}
	Serial.println();
#endif
}

bool Message::push(byte byteVal)
{
	if (enoughRoom(sizeof(byteVal)))
	{
		payload[payloadLength++] = byteVal;
		return true;
	}
	return false;
}

bool Message::push(int intVal)
{
	if (enoughRoom(sizeof(intVal)))
	{
		//Now write the low byte
		payload[payloadLength++] = lowByte(intVal);

		//First write the high byte
		payload[payloadLength++] = highByte(intVal);

		return true;
	}
	return false;
}

bool Message::push(char charVal)
{
	if (enoughRoom(sizeof(charVal)))
	{
		payload[payloadLength] = charVal;
		payloadLength += sizeof(charVal);
		return true;
	}
	return false;
}

byte Message::popByte()
{
	//as it is just a byte, it is quite straight forward
	byte result = payload[pos++];

	return result;
}

int Message::popInt()
{
	//The payload should have: |HIGH|LOW|
	//As we read from right to left, we first red the low byte, and then the high byte
	byte high = payload[pos++];
	byte low = payload[pos++];

	int result = word(low, high);

	return result;
}

char Message::popChar()
{
	//as a char has the same size of a byte, it is quite straight forward
	char result = payload[pos++];

	return result;
}

bool Message::enoughRoom(int requestedSize)
{
	int remaining = MAX_MESSAGE_LENGTH - payloadLength;

	bool result = remaining >= requestedSize;

#ifdef DEBUG
	if (!result){
		Serial.print("Error: Not enought space in message for param. Requested: ");
		Serial.print(requestedSize);
		Serial.print(" Remaining: ");
		Serial.println(remaining);
	}
#endif

	return result;
}

byte Message::getTotalLength()
{
	return HEADER_LENGTH + payloadLength;
}

byte* Message::getBytes()
{
	byte msgLength = getTotalLength();

	//Create a buffer that can hold the message (type byte + length byte + payload bytes)
	byte msg[msgLength];

	msg[0] = type;
	msg[1] = payloadLength;
	for (int i = 0; i < payloadLength; i++)
	{
		//+2 because the first two bytes are the type and the payloadLength 
		msg[i + HEADER_LENGTH] = payload[i];
	}
}

bool Message::payloadAvailable()
{
	return (payloadLength - pos) > 0;
}