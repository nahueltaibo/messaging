#define DEBUG
#include "Message.h"


void setup() {
	// use the serial port to monitor that things work
	Serial.begin(9600);
	Serial.println("ready");
}

void loop() {

	Message test(PUBLISH);
	test.push((int)-32768);
	test.push((int)32767);

	test.push((byte)255);
	test.push((byte)0);

	test.push((char)'0');
	test.push((char)'Z');

	test.debug(false);

	int intVar = test.popInt();
	Serial.print("var: ");
	Serial.println(intVar);

	intVar = test.popInt();
	Serial.print("var: ");
	Serial.println(intVar);

	byte byteVar = test.popByte();
	Serial.print("var: ");
	Serial.println(byteVar);

	byteVar = test.popByte();
	Serial.print("var: ");
	Serial.println(byteVar);

	char charVar = test.popChar();
	Serial.print("var: ");
	Serial.println(charVar);

	charVar = test.popChar();
	Serial.print("var: ");
	Serial.println(charVar);
	delay(10000);
}

